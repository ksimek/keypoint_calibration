#!/usr/bin/env python

import argparse
from features_ import *

def main():
    parser = argparse.ArgumentParser(description="Calibrate using keypoint matches");
    parser.add_argument('pattern_key_fname', type=str, help='reference pattern keypoint file')
    parser.add_argument('num_frames', type=int, help='Number of scene frames')
    parser.add_argument('scene_key_fmt', type=str, help='Printf-formatted string representing keypoint files from N scene frames')
    parser.add_argument('match_fmt', type=str, help='Printf-formatted string representing keypoint match files')
    parser.add_argument('out_fname', type=str, help='filename for intrinsic calibrated camera')

    args = parser.parse_args()

    pattern_keys = read_features(arg.pattern_fname);

    frames_keys = map(lambda x: read_features(arg.scene_fmt % x), range(1, arg.num_frames+1))
    frames_matches = map(lambda x: read_matches(arg.match_fmt % x), range(1, arg.num_frames+1))

    [obj_pts, image_pts] = to_calibration_data(frames_matches, pattern_keys frames_keys)

    cam = calibrate_intrinsic(obj_pts, image_pts)

    write_intrinsic_camera(arg.out_fname, cam);

if __name__ == "__main__":
    main();
