from cv2 import *
import numpy as np

def read_features(in_fname):
    infile = open(in_fname, 'r')
    tokens = infile.readline().split()
    assert(len(tokens) == 2)
    num_pts = int(tokens[0])
    num_dims = int(tokens[1])

    descs = np.zeros((num_pts, num_dims), np.float32)

    kps = []

    for i in range(0, num_pts):
        tokens = infile.readline().split()
        assert(len(tokens) == 4)

        x = float(tokens[0])
        y = float(tokens[1])
        sz = float(tokens[2])
        angle = float(tokens[3])

        kps.append(KeyPoint(x, y, sz, angle))

        num_read = 0
        while num_read < num_dims:
            tokens = infile.readline().split()
            descs[i, num_read:(num_read+len(tokens))] = np.array(map(int, tokens))
            num_read += len(tokens)

    return (kps, descs)

def write_features(out_fname, kps, descs):
    outfile = open(out_fname, 'w')
    num_pts = len(kps)
    num_dims = descs.shape[1]
    assert(descs.shape[0] == num_pts)

    line = "%d %d" % (num_pts, num_dims)
    outfile.write(line)
    
    for i in range(0, num_pts):
        kp = kps[i]
        desc = descs[i,:]

        outfile.write("\n%f %f %f %f" % (kp.pt[0], kp.pt[1], kp.size, kp.angle))
        for i in range(0, num_dims):
            if i % 20 == 0:
                outfile.write('\n')
            else:
                outfile.write(' ')
            outfile.write('%d' % desc[i])

def extract_features(fname, use_surf=True, upright=False):
    if use_surf:
        feat = SURF(400, 4, 2, True, upright)
    else:
        feat = SIFT()

    print fname
    img = imread(fname, False)
    if img is None:
        return (None, None)
    [kp, desc] = feat.detectAndCompute(img,None)
    print "%d keypoints found" % len(kp)

    return (kp, desc)

def match_features(desc1, desc2, lowe_ratio=0.6):
    if desc1.shape[1] != desc2.shape[1]:
        raise Exception("incompatible feature vector dimensions")

    bf = BFMatcher(NORM_L2)
    matches = bf.knnMatch(desc1, desc2, k=2)

    distRatio = 0.6
    good_matches = []
    for m,n in matches:
        if m.distance < distRatio * n.distance:
            good_matches.append(m)

    return good_matches

def filter_matches_by_homography(key_pts1, key_pts2, matches, threshold):
    N = len(matches)
    src_pts = np.zeros((N, 2))
    dst_pts = np.zeros((N, 2))
    i = 0
    for match in matches:
        i1 = match.queryIdx;
        i2 = match.trainIdx;
        src_pts[i, :] = key_pts1[i1,:]
        dst_pts[i, :] = key_pts2[i2,0:2]
        i += 1

    N = len(matches)
    [H, mask] = findHomography(src_pts, dst_pts, RANSAC, threshold)

    mask = np.nonzero(np.reshape(mask, (-1)))[0]
    return [matches[i] for i in mask]

def write_matches(fname, matches):
    f = open(fname, 'w')
    
    f.write("%d" % len(matches))

    for i in range(0, len(matches)):
        f.write("\n%d %d %f" % (m.queryIdx, m.trainIdx, m.distance))

def read_matches(fname):
    f = open(fname, 'r')
    tokens = f.readline().split()
    assert(len(tokens) == 1)
    num_matches = int(tokens[0])

    matches = []
    for i in range(0, num_matches):
        tokens = f.readline().split()
        assert(len(tokens) == 3)
        matches.append(DMatch(int(tokens[0]), int(tokens[1]), float(tokens[2])))

    return matches
